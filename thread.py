import threading
import time


def thread1():
    for count in range(11):
        print("Thread 1 counts" + count)
        time.sleep(1)


def thread2():
    for count in range(10, 0, -1):
        print("Thread 2 counts" + count)
        time.sleep(1)


if __name__ == "__main__":
    thread_1 = threading.Thread(target=thread1)
    thread_2 = threading.Thread(target=thread2)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()
